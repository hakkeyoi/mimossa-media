<?php get_header(); ?>

<div id="body">

    <section class="body_wrapper">
      
        <section class="category_header">
            <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
                <?php if(function_exists('bcn_display')){ bcn_display(); } ?>
            </div>
          <!-- パンくず -->
          <h2 class="sawarabi index_title_h2">
            <?php
              if( is_category() || is_tag() || is_tax()):
                single_tag_title();
              elseif(is_post_type_archive('news')):
                echo "お知らせ";
              elseif (is_search()):
                echo "検索結果";
              else:
                echo "記事一覧";
              endif;
            ?>
          </h2>
        </section>

      

        <section id="index" class="index-page">
            <ul class="flex_cont_cg">
            <?php if(have_posts()): while( have_posts() ): the_post(); ?>
              <li>
                  <dl class="flex_item_cg">
                    <dt>
                    <?php if ( has_post_thumbnail() ): the_post_thumbnail(); else: ?>
                          <img src="<?php echo get_template_directory_uri();?>/img/media/noimage_lg.png">
                      <?php endif; ?>
                    </dt>
                    <dd class="sawarabi">
                        <p><?php the_time('Y.m.d'); ?></p>
                        <h3 class="index_h3"><?php the_title(); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                        <p><a class="index-link" href="<?php the_permalink(); ?>">詳しくはこちら</a></p>
                    </dd>
                  </dl>
              </li>
              <?php endwhile;else: ?>
              <?php endif; ?>
            </ul>
        </section>
    </section>


      
    <div id="pc_copy" class="section_block">
      ©2020 mimossa
    </div><!-- pc_copy -->

</div><!-- #body -->

<?php get_footer(); ?>



