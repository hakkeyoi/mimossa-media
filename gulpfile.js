const { src, dest, watch } = require('gulp');
const sass = require('gulp-sass');


function developmentWatch(){
    return watch('./sass/style.scss',function(){
        return src('./sass/style.scss')
        .pipe(sass())
        .pipe(dest('./'));
    });

}


function production(){
    return src('./sass/style.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(dest('./'));
}


function fronttWatch(){
    return watch('./sass/front.scss',function(){
        return src('./sass/front.scss')
        .pipe(sass())
        .pipe(dest('./css/'));
    });

}


function productionFront(){
    return src('./sass/front.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(dest('./css/'));
}


exports.ftdev = fronttWatch;

exports.ftprod = productionFront;

// exports.build = developmentWatch;
exports.default = production;