<?php

add_action('init','create_newsposts');

add_theme_support('post-thumbnails'); 

function create_newsposts(){
  register_post_type('news',
    array(
      'labels' => array(
        'name' => _('ニュース'), // 投稿タイプの一般名
        'singluar_name' => _('newspost'), // nameの単数形
        'menu_name' => _('ニュース'), // 表示名
        'add_new' => _('新しいニュースを投稿'),
        'show_admin_column' => true,
      ),
      'public' => true,
      'menu_position' => 5,
      'rewrite' => array( 'slug' => 'news' ),
      'has_archive' => true,
      'public' => true,
      'supports' => array('title','excerpt','thumbnail','editor','thumbnail')
    )
  );
};


