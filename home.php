<?php	
/*
Template Name: homephp
*/
get_header();
?>

<div id="body">
	<section class="body_wrapper">
		<ul>
			<?php
				$args = array();
				$my_posts = get_posts( $args );
				global $post;
				foreach( $my_posts as $post ){ setup_postdata( $post );
			?>
			<?php get_template_part('postlist');  ?>
			<?php } wp_reset_postdata(); ?>
		</ul>
	</section>

	<div id="pc_copy" class="section_block">
	©2020 mimossa
	</div><!-- pc_copy -->

</div><!-- #body -->

<?php get_footer(); ?>


