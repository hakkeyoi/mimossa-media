
<li>
    <div class="main_image">
        <a href="<?php the_permalink(); ?>">
            <?php
                if ( has_post_thumbnail() ):
                    the_post_thumbnail();
                else:
            ?>
                <img src="<?php echo get_template_directory_uri();?>/img/media/noimage_lg.png">
            <?php endif; ?>
        </a>
    </div>
    <div class="body_wrapper">
    <p class="main_date sawarabi">2020.03.19</p>
    <h2 class="sawarabi"><?php the_title(); ?></h2>
    <p class="p_b_block"><?php echo get_the_excerpt(); ?></p>
    <p class="bt_detail">
        <a href="<?php the_permalink(); ?>" class="bt_detail_link">詳しくはこちら<span class="arrow_r"></span></a>
    </p>
    </div>
</li>


