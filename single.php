<?php get_header(); ?>

<div id="body">
    <?php if( have_posts() ): while(have_posts()): the_post(); ?>
        <p class="main_image">
  
            <?php if ( has_post_thumbnail() ): the_post_thumbnail(); else:?>
                <img src="<?php echo get_template_directory_uri();?>/img/media/noimage_lg.png">
            <?php endif; ?>
        </p>
        <div class="body_wrapper">

            <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
                <?php if(function_exists('bcn_display')){ bcn_display(); } ?>
            </div>
            
            <p class="main_date sawarabi"><?php the_date("Y.m.d"); ?></p>
            <div class="single_page">
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>

            </div>


            <?php endwhile;else: ?>
            投稿はありません
        <?php endif; ?>
    </div>
	<?php get_template_part( "bottom" ); ?>
</div>

<?php get_footer(); ?>
