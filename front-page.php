<?php get_header('corp'); ?>

<section class="mv">
    <div class="inner">
        <h1>
            <img class="only_pc" src="<?php echo get_template_directory_uri();?>/img/front/fv_ttl_pc.png" alt="Mimossa Co., Ltd.">
            <img class="only_sp" src="<?php echo get_template_directory_uri();?>/img/front/fv_ttl_sp.png" alt="Mimossa Co., Ltd.">
        </h1>
        <p>
            <img class="only_pc" src="<?php echo get_template_directory_uri();?>/img/front/fv_txt_pc.png" alt="伴奏 心とカラダ、変化するライフステージに寄り添う">
            <img class="only_sp" src="<?php echo get_template_directory_uri();?>/img/front/fv_txt_sp.png" alt="伴奏 心とカラダ、変化するライフステージに寄り添う">
        </p>
    </div>
</section>


<!-- our service -->
<section class="front-service" id="service">
    <div class="inner">
        <h2 class="h2_ttl"><img src="<?php echo get_template_directory_uri();?>/img/front/h2_svs.png" alt=""></h2>

        <div class="service_1">
            <img class="only_sp" src="<?php echo get_template_directory_uri();?>/img/front/svs_bg_1_sp.png">
            <div class="cnt">
                <h3><img src="<?php echo get_template_directory_uri();?>/img/front/svs_h3_1.png" alt=""></h3>
                <p>CARINA GINZA〈カリーナギンザ〉<br>東京都中央区銀座一丁目6番6号。高い技術と豊富な知識を誇るスパニストが在籍するヘッドスパ専門店。白髪や抜け毛、ダメージヘアなど、お客様の悩みに寄り添いヘッドケアします。</p>
                <a class="svs-link" href="http://carina-ginza.com/" target="_blank">詳しくはこちら</a>
            </div>
        </div>

        <div class="service_2">
        <img class="only_sp" src="<?php echo get_template_directory_uri();?>/img/front/svs_bg_2_sp.png">
            <div class="cnt">
                <h3><img src="<?php echo get_template_directory_uri();?>/img/front/svs_h3_2.png" alt=""></h3>
                <p>EGHONEY〈イージーハニー〉<br>サロン品質でホームケアできる頭皮用保湿パック。良質の素材だけを厳選し作り上げたプロフェッショナルプロダクトの販売。</p>
                <a class="svs-link" href="https://eghoney.jp/" target="_blank">詳しくはこちら</a>
            </div>
        </div>
    </div><!--inner--->
</section>


<!-- Contents -->
<section class="front-contents" id="contents">
    <div class="inner">
        <h2 class="h2_ttl"><img src="<?php echo get_template_directory_uri();?>/img/front/h2_cnt.png" alt=""></h2>
        <h3 class="h3_one media"><img src="<?php echo get_template_directory_uri();?>/img/front/content_h3.png" alt=""></h3>
        <div class="content-posts">
            <ul class="front-post-list">
                <?php
                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => 4
                    );
                    $the_query = new WP_Query( $args );
                    if ( $the_query->have_posts() ):
                    while ( $the_query->have_posts() ) : $the_query->the_post();
                ?>

                <li>
                    <div class="thumb">
                        <?php
                            if ( has_post_thumbnail() ):
                                the_post_thumbnail();
                            else:
                        ?>
                            <img src="<?php echo get_template_directory_uri();?>/img/media/noimage_lg.png" alt="">
                        <?php endif; ?>
                    </div>
                    <div class="post_text">
                        <p class="date"><?php the_date("Y.m.d"); ?></p>
                        <h4><?php the_title(); ?></h4>
                        <p><?php the_excerpt(); ?></p>
                    </div>
                    <a href="<?php the_permalink(); ?>">詳しくはこちら</a>
                </li>

                <?php endwhile; wp_reset_postdata(); else:?>

                <p>記事がありません</p>
                <?php endif; ?>
            </ul>
        </div>
    </div><!--inner--->
</section>


<!-- Company -->
<section class="front-company" id="profile">
    <div class="inner">
        <h2 class="h2_ttl"><img src="<?php echo get_template_directory_uri();?>/img/front/h2_cmp.png" alt=""></h2>
        <div class="company-cnt">
            <h3 class="h3_one"><img src="<?php echo get_template_directory_uri();?>/img/front/company_h3.png" alt=""></h3>
            <p class="txt">ミモッサは、ミモザの花を表しており、イタリア語でミモザの花は【感謝】や【思いやり】の意味が込められています。<br>弊社もすべての方々に【感謝】や【思いやり】の心情を持って取り組んでいくことから名付けられました。</p>
            <dl class="company-dl">
                <dt>社名</dt>
                <dd>ミモッサ株式会社 / MIMOSSA CO., LTD.</dd>
            </dl>
            <dl class="company-dl">
                <dt>住所</dt>
                <dd>東京都中央区銀座1丁目6番6号</dd>
            </dl>
            <dl class="company-dl">
                <dt>代表者名</dt>
                <dd>代表取締役　横関 順子</dd>
            </dl>
            <dl class="company-dl">
                <dt>事業内容</dt>
                <dd>ヘッドスパ専門店CARINA GINZA店舗運営<br>D2C事業（自社商品の開発～販売）</dd>
            </dl>
            <dl class="company-dl">
                <dt>会社設立</dt>
                <dd>2012年 10月</dd>
            </dl>
            <dl class="company-dl">
                <dt>サイトURL</dt>
                <dd><a>https://mimossa.co.jp</a></dd>
            </dl>
        </div>
    </div><!--inner--->
</section>


<!-- Contact -->
<section class="front-contact" id="contact">
    <div class="inner">
        <h2 class="h2_ttl"><img src="<?php echo get_template_directory_uri();?>/img/front/h2_ctt.png" alt=""></h2>
        <h3 class="h3_one"><img src="<?php echo get_template_directory_uri();?>/img/front/contact_h3.png" alt=""></h3>
        <div class="form-wrap">
            <?php echo do_shortcode('[contact-form-7 id="8" title="contact"]'); ?>
        </div>
    </div>
</section>


<!-- bottom contents -->
<section class="front-bottom">
    <div class="inner clearfix">
        <div class="front-news">
            <h2 class="h2_ttl"><img src="<?php echo get_template_directory_uri();?>/img/front/h2_news.png" alt=""></h2>
            <h3 class="h3_one"><img src="<?php echo get_template_directory_uri();?>/img/front/news_h3.png" alt=""></h3>
            <div class="front-news-list">
                <ul>

                    <?php
                        $args = array(
                            'post_type' => 'news',
                            'posts_per_page' => 5
                        );
                        $the_query = new WP_Query($args);
                        if($the_query->have_posts()):
                        while($the_query->have_posts()):  $the_query->the_post();
                    ?>

                        <li><a href="<?php the_permalink();?>">
                            <dl>
                                <dt><?php the_time("Y.m.d"); ?></dt>
                                <dd><?php the_title(); ?></dd>
                            </dl>
                            </a>
                        </li>
                    
                        <?php endwhile; wp_reset_postdata(); else: ?>
                        
                        <li>news投稿なし</li>
                    
                    <?php endif;?>

                </ul>
            </div>
        </div>
        <div class="front-instagram">
            <h2 class="h2_ttl"><img src="<?php echo get_template_directory_uri();?>/img/front/h2_instagram.png" alt=""></h2>
            <h3 class="h3_one"><img src="<?php echo get_template_directory_uri();?>/img/front/instagram_h3.png" alt=""></h3>
            <div class="">
                <?php //get_template_part( 'instagram' ); ?>
            </div>
        </div>
    </div>
</section>

</main>


<footer class="footer">
    <div class="inner">
        <div class="footer-menu">
            <ul>
                <li><a href="#service">Our Service</a></li>
                <li><a href="#contents">Contens</a></li>
                <li><a href="#profile">Company Profile</a></li>
                <li><a href="#contact">Contact Us</a></li>
            </ul>
            <ul>
                <li><a href="/privacy">プライバシーポリシー</a></li>
                <li><a href="/terms">利用規約</a></li>
            </ul>
        </div>
        <p>MIMOSSA</p>
        <p><small>&copy;copy right</small></p>
    </div>
</footer>
