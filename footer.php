
    <!-- sidebar -->
    <div id="topics">
      <section class="topics_article">
          <form id="form" action="/" method="get">
            <div class="search-box">
              <input type="text" name="s">
              <button type="submit">
                <img src="<?php echo get_template_directory_uri();?>/img/media/search.png" width="12">
                <span>検索</span>
              </button>
            </div>
          </form>
        </section>
      <section class="topics_article">
        <h2 class="topics_title sawarabi">
          新着記事
        </h2>
        <ul>
          
          <?php 
            $args = array(
            'post_type' => 'post',
            'posts_per_page' => 5
            );
            $the_query = new WP_Query($args);
            if($the_query->have_posts()):
            while($the_query->have_posts()):  $the_query->the_post();
          ?>
          <li>
            <dl>
              <dt class="sawarabi"><?php the_date("Y.m.d");?></dt>
              <dd>
                <a href="<?php the_permalink();?>"><?php the_title();?></a>
              </dd>
            </dl>
          </li>

          <?php endwhile;wp_reset_postdata();else: ?>
            <li>
              <dl>
              投稿はありません
              </dl>
            </li>
          <?php endif; ?>
        </ul>
      </section>

      <section class="topics_article">
        <h2 class="topics_title sawarabi">
          人気記事
        </h2>
        <ul>
        <?php
          $args = array('post_html' => '<li><dl><dt class="sawarabi">{date}</dt><dd><a href="{url}">{text_title}</a></dd></dl></li>');
          wpp_get_mostpopular($args);
        ?>
        </ul>
      </section>


      <section class="topics_article">
        <h2 class="topics_title sawarabi">カテゴリー</h2>
        <ul class="category_wrap">
          <?php
              $categories = get_terms( 'category', "fields=all&get=all&exclude=1" );
              foreach( $categories as $cat):
            ?>
              <li class="category_item">
                <a href="<?php echo get_tag_link($cat->term_id); ?>">
                  <?php echo $cat->name; ?>
                </a>
                </li>
            <?php endforeach; ?>
        </ul>
      </section>


      <section class="topics_article">
        <h2 class="topics_title sawarabi">タグ</h2>
        <div class="sidebar-tags">
          <ul>
            <?php
              $tags = get_terms( "post_tag", "fields=all&get=all" );
              foreach( $tags as $tag):
            ?>
              <li>
                <a href="<?php echo get_tag_link($tag->term_id); ?>">
                  <?php echo $tag->name; ?>
                </a>
                </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </section>
      

      <section>
        <div id="smp_copy">
          ©2020 mimossa
        </div>
      </section>
    </div><!-- #topics -->
    
</main><!-- .content -->

</div><!-- .wrapper -->
