<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://fonts.googleapis.com/css?family=Sawarabi+Mincho" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/front.css?<?php echo time(); ?>">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/front.js"></script>
</head>
<body class="fadeout">

<div id="corporate">

<header class="corp-header">
    <div class="corp-header-inner">
        <p><a class="sawarabi" href="/">
            <img src="<?php echo get_template_directory_uri();?>/img/front/logo.png" alt="Mimossa Co., Ltd.">
        </a></p>
        <nav class="global-nav">
            <ul>
                <li><a href="#service">Our Service</a></li>
                <li><a href="#contents">Contens</a></li>
                <li><a href="#profile">Company Profile</a></li>
                <li><a href="#contact">Contact Us</a></li>
            </ul>
        </nav>

        <div id="sp-front-btn" class="toggle_btn">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
</header>


<div id="sp-front-menu" class="front-menu">
    <div id="close-btn">
        <span class="batsu"></span>
    </div>
    <nav>
        <ul class="sp-menu">
            <li><a href="#service">Our Service</a></li>
            <li><a href="#contents">Contens</a></li>
            <li><a href="#profile">Company Profile</a></li>
            <li><a href="#contact">Contact Us</a></li>
        </ul>
    </nav>
</div>

<main>
